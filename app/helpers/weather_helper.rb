module WeatherHelper
  def weather_icon(icon)
    "http://openweathermap.org/img/wn/#{icon}@2x.png"
  end
end
