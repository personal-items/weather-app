require 'net/http'

class WeatherController < ApplicationController
  APP_ID = Rails.application.credentials.weather_api_key
  COORDINATES_URL = "https://api.openweathermap.org/geo/1.0/zip".freeze
  WEATHER_URL = "https://api.openweathermap.org/data/2.5/weather".freeze

  def index
  end

  def search
    ##
    # send postal_code to geocoding API to get the necessary cooridates (latitude longitude)
    # to the weather api
    postal_code_response = find_coordinates_by_postal_code(params[:postal_code])

    if postal_code_response['cod'].present? && postal_code_response['cod'] != '200'
      flash[:alert] = postal_code_response['message']
      return render action: :index
    end
    
    if Rails.cache.fetch(params[:postal_code]).present?
      flash.now[:alert] = "Weather data retrieved from cache"
      @weather_response = Rails.cache.read(params[:postal_code])
    else
      @weather_response = Rails.cache.fetch(params[:postal_code], expires_in: 30.minutes) do
        get_weather(lat: postal_code_response["lat"], lon: postal_code_response["lon"])
      end
    end
  end

  private

    def api_request(url:, params: {})
      uri = URI(url)
      uri.query = URI.encode_www_form(params)
      response = Net::HTTP.get_response(uri)
      JSON.parse(response.body)
    end

    def find_coordinates_by_postal_code(postal_code)
      api_request(url: COORDINATES_URL, 
                  params: { zip: postal_code, appid: Rails.application.credentials.weather_api_key })
    end

    def get_weather(lat:, lon:)
      api_request(url: WEATHER_URL, params: { lat: lat, lon: lon, units: "imperial", appid: APP_ID })
    end
end
