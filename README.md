# README

Weather App

Building a weather app to satisfy a coding assignment

- Must be done in Ruby on Rails
  - I decided to use the latest rails: Rails 7.0.4
- Accept an address as input
  - I didn't see the need to capture the entire address when all I need is the postal code.
- Retrieve forecast data for the given address. This should include, at minimum, the
current temperature (Bonus points - Retrieve high/low and/or extended forecast)
  - I added the temperature and high/low
- Display the requested forecast details to the user
  - I display the forecast details to the user in the search.html.erb file
- Cache the forecast details for 30 minutes for all subsequent requests by zip codes. Display indicator if result is pulled from cache.
  - I decided to use Rails built in caching for low-level caching (https://guides.rubyonrails.org/caching_with_rails.html#low-level-caching). Caching expires in 30 mins, and an alert will indicate wether or not the weather data was pulled from cache or not

* Ruby version
ruby 3.1.0p0
