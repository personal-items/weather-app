require "test_helper"

class WeatherControllerTest < ActionDispatch::IntegrationTest
  test "should get index" do
    get root_path
    assert_response :success
  end

  test "should get postal_code search" do
    get search_path, params: { postal_code: "90210" }
    assert_response :ok
    assert_select "strong", text: "Temp"
  end

  test "should alert user with error invalid zip code" do
    get search_path, params: { postal_code: "4" }
    assert_select "div", text: "invalid zip code"
  end
end
